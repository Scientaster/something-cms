import React from 'react';
import * as Bootstrap from 'react-bootstrap';
import * as ComponentCMS from '../index';
import 'bootstrap/dist/css/bootstrap.min.css';

const cmsLibrary = {
	'react-bootstrap' : Bootstrap,
	'component-cms-lib': ComponentCMS
}

const exampleStoryData = {
	components: [
		{
			library: 'react-bootstrap',
			component: 'Container',
			props: {},
			children: [
				{
					library: 'react-bootstrap',
					component: 'Alert',
					props: {
						variant: 'success'
					},
					children: [
						{
							library: 'component-cms-lib',
							component: 'Span',
							props: {
								value: 'Alert 123'
							},
							children: []
						}
					]
				},
				{
					library: 'react-bootstrap',
					component: 'Button',
					props: {
						variant: 'success'
					},
					children: [
						{
							library: 'component-cms-lib',
							component: 'Span',
							props: {
								value: 'Testing 123'
							},
							children: []
						}
					]
				}
			]
		}
	]
};

export const ExampleUsageStory = () => (
	<ComponentCMS.ComponentLibrary.Provider value={cmsLibrary}>
		<ComponentCMS.default prefetchedData={exampleStoryData} />
	</ComponentCMS.ComponentLibrary.Provider>
);


export default {
  title: 'Example usage',
};
