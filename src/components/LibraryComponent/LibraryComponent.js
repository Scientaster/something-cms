import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import ComponentLibrary from '../ComponentLibrary';

const LibraryComponent = ({ library, component, props, children }) => {
	const myLibrary = useContext(ComponentLibrary)[library];
	const MyComponent = myLibrary[component];

  return (
    <MyComponent {...props}>
    	{children.map(component => (
    		<LibraryComponent {...component} />
  		))
    	}
    </MyComponent>
  );
};

LibraryComponent.propTypes = {
  library: PropTypes.string
};

export default LibraryComponent;
