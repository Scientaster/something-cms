import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import LibraryComponent from '../LibraryComponent';

const ComponentRoot = ({ prefetchedData, id }) => {
		const [data, setData] = useState(prefetchedData);
		useEffect(() => {
			if (!data) {
				fetch(`localhost:3000/root/${id}`).then(response => {
					setData(response.body.json());
				});
			}
		})

    return (
        data.components.map(component => (
        		<LibraryComponent {...component} />
      		)
        )
    );
};

ComponentRoot.displayName = 'ComponentRoot';

ComponentRoot.propTypes = {
    className: PropTypes.string,
};

export default ComponentRoot;
