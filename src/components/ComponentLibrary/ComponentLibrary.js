import React from 'react';

const LibraryContext = React.createContext({
	libraries: []
});

export default LibraryContext;