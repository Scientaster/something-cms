import React from 'react';
import PropTypes from 'prop-types';

const Span = ({ value }) => {
    return (
        <span>{value}</span>
    );
};

Span.displayName = 'Span';

Span.propTypes = {
    value: PropTypes.any,
};

export default Span;
